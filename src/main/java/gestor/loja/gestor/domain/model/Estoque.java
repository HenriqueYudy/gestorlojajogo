package gestor.loja.gestor.domain.model;

import java.util.Date;

public class Estoque {

	private long idestoque;
	private long idfornecedor;
	private long idfuncionario;
	private long idjogo;
	private double valortotal;
	private String vendido;
	private Date datarecebimento;
	private Date datavencimento;

	@Override
	public String toString() {
		return "Estoque [idestoque=" + idestoque + ", idfornecedor=" + idfornecedor + ", idfuncionario=" + idfuncionario
				+ ", idjogo=" + idjogo + ", valortotal=" + valortotal + ", vendido=" + vendido + ", datarecebimento="
				+ datarecebimento + ", datavencimento=" + datavencimento + "]";
	}

}

package gestor.loja.gestor.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Vendas {

	private long id;

	private String cliente;

	private long funcionario;

	private long jogo;

	private BigDecimal valor;

	private int quantidade;

	private BigDecimal valor_total;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public long getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(long funcionario) {
		this.funcionario = funcionario;
	}

	public long getJogo() {
		return jogo;
	}

	public void setJogo(long jogo) {
		this.jogo = jogo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getValor_total() {
		return valor_total;
	}

	public void setValor_total(BigDecimal valor_total) {
		this.valor_total = valor_total;
	}

	@Override
	public String toString() {
		return "Vendas [id=" + id + ", cliente=" + cliente + ", funcionario=" + funcionario + ", jogo=" + jogo
				+ ", valor=" + valor + ", quantidade=" + quantidade + ", valor_total=" + valor_total + "]";
	}

}

package gestor.loja.gestor.domain.resource;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import gestor.loja.gestor.domain.model.Vendas;

@RestController
@RequestMapping(value = "/realizar_venda")
public class VendasResource {

	@Autowired
	private RestTemplate restTemplate;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/servicoadd")
	public ResponseEntity<Vendas> realizarVenda(@Valid @RequestBody Vendas venda) {

		String url = "http://127.0.0.1:2222/vendas/add";
		System.out.println(venda);

		ResponseEntity<Vendas> vendasResponse = restTemplate.postForEntity(url, venda, Vendas.class);

		return ResponseEntity.status(HttpStatus.CREATED).body(venda);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/servico/all")
	public ResponseEntity<List<Vendas>> buscarVendas() {

		String url = "http://127.0.0.1:2222/vendas/getall";

		ResponseEntity<List<Vendas>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Vendas>>() {
				});

		List<Vendas> vendasLst = response.getBody();

		return new ResponseEntity<List<Vendas>>(vendasLst, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/servico/get/{id}")
	public ResponseEntity<Vendas> buscarVenda(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:2222/vendas/get/" + id;

		ResponseEntity<Vendas> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<Vendas>() {
				});

		Vendas venda = response.getBody();

		return new ResponseEntity<Vendas>(venda, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/servico/update/{id}")
	public ResponseEntity<Vendas> atualizarVenda(@PathVariable("id") long id, @RequestBody Vendas venda) {

		String url = "http://127.0.0.1:2222/vendas/update/" + id;

		HttpEntity<Vendas> requestEntity = new HttpEntity<>(venda);

		ResponseEntity<Vendas> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Vendas.class);

		Vendas vendasAtualizada = response.getBody();

		return new ResponseEntity<Vendas>(vendasAtualizada, HttpStatus.OK);

	}

	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "servico/delete/{id}")
	public ResponseEntity<Vendas> atualizarVenda(@PathVariable("id") long id) {

		String url = "http://127.0.0.1:2222/vendas/delete/" + id;
		restTemplate.delete(url);

		return new ResponseEntity<Vendas>(HttpStatus.OK);

	}

}
